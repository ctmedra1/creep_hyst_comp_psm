Code associated to the article :

Creep and Hysteresis Compensation in Pressure Sensitive Mats for 
Improving Center-of-Pressure Measurements

by Javier Martinez-Cesteros, Carlos Medrano-Sanchez, 
Julian Castellanos-Ramos, Josa A. Sanchez-Duran, and 
Inmaculada Plaza-Garcia

accepted for publication in IEEE Sensors Journal

If you have downloaded all the files and data in the same directory:

main.py
hystCreepModels.py
hystModels.py
creepModels.py
copExperiment.py 
pneumaticExperiment.py

data/
results/

and you run:

python main.py

then the main results of the article are printed in the standard output
or stored in the results directory. Data are taken from the directory
data

Numpy, scipy and matplotlib are required to be installed.

In the results directory you will get the figures 5 and 6.

Model parameters are printed in the standar output.

Eu for all the volunteers is printed in the standard output. A summary 
of the Eu results is printed in the standard output at the end. It is
the information given in figure 8 with the name "Total". The other
results of the figure and of the paper can be deduced from the values
printed in the standard output.
