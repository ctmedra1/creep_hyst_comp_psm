#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright 2023 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""

import sys

import numpy as np
import pandas as pd
import pickle
import json


import hystModels
import creepModels
import hystCreepModels
##


PFACTOR = 32*6894.75729 # 32 psi to Pa
LX = 32e-2/16
LY = 32e-2/16 # 32 cm 16 cells

def pressure2Cop(p):
    '''
    It calculate the Ceter of Pressure when the input
    pressure is  matrix NxM
    '''
    #import ipdb;ipdb.set_trace()
    NY, NX = p.shape[0], p.shape[1]
    X, Y = np.meshgrid(np.arange(0, NX), np.arange(0, NY))
    ptotal = p.sum()
    x = (X*p).sum()/ptotal
    y = (Y*p).sum()/ptotal
    return x, y
##
def pressure2CopSeries(p, d = 1.0):
    '''
    Just a convenience function to call pressure2Cop for each time.
    p is TxNxM T temporal
    d: distance between cells. Just multiplicative factor
    '''
    x = np.zeros(p.shape[0])
    y = np.zeros(p.shape[0])
    for tt in range(p.shape[0]):
        x[tt], y[tt] = pressure2Cop(p[tt,:,:])
    return d*x, d*y
##
