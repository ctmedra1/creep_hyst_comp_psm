import numpy as np
import sys


class CreepOpArray:
    """
    Weighted sum of creep operators 
    The input should be normalized in [0, 1]
    """
    def __init__(self, l, w, T,  yold=0.0):
        # Better if the order in l is in decreasing order
        # A constant term is just setting l as np.inf
        self.l = l
        self.w = w
        self.yold = np.zeros(l.shape[0])
        if((type(yold)==float or type(yold)==np.float64)):            
            self.yold[:] = yold
        else:
            self.yold[:] = yold[:]
        self.a1 = np.exp(-l*T)
        self.a2 = (1-self.a1)
        self.xold = 0.0 # Assume starting at 0 for inverse
    ##
    def update(self, valin, inverse):
        single = (type(valin)==float or type(valin)==np.float64)
        if (single):
            val = np.array([valin]) # A trick: everything is an array
        else:
            val = valin
        if(not inverse):
            xold = val # Just a proper name
            ytotal = np.zeros(xold.shape[0])
            for tt in range(xold.shape[0]):
                # In fact, y is at tt+1 if you were writing
                # the equation in paper, but at the end
                # we get an array anyway. It is OK
                ydum = self.a1*self.yold + self.a2*xold[tt]
                self.yold[:] = ydum[:]
                ytotal[tt] = (self.yold*self.w).sum()
            #self.ytotal = ytotal # No need to save ytotal
            if(single):
                return ytotal[-1]
            else:
                return ytotal
        else:
            # Inverse. Assume first is the faster creep op
            yold = val # Better name. It is the global input of inverse
            # Do not confuse yold with self.yold 
            # (the internal state of each creep operator)
            xold = np.zeros(yold.shape[0]) # The output of the inverse
            for tt in range(yold.shape[0]):
                u = yold[tt]
                for nn in range(1, self.l.shape[0]):
                    ydum = self.a1[nn]*self.yold[nn] + self.a2[nn]*self.xold
                    self.yold[nn] = ydum
                    u -= ydum*self.w[nn] # Contribution of the nn operator
                ubis = u/self.w[0]
                xdum = (ubis-self.a1[0]*self.yold[0])/self.a2[0]
                self.yold[0] = ubis
                xold[tt] = xdum
                # Xold is a unique value for all. The same branch in the
                # block diagram
                self.xold = xold[tt]
            if(single):
                return xold[-1]
            else:
                return xold
    ##
    def reset(self, yold = 0.0):
        self.xold = 0.0 # Assume starting at 0
        if((type(yold)==float or type(yold)==np.float64)):            
            self.yold[:] = yold
        else:
            self.yold[:] = yold[:]
        return
##
