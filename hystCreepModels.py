"""
Copyright 2023 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import sys
import numpy as np
import copy
from scipy.optimize import minimize, basinhopping

class HystCreepAdd:
    # optimized for values in (0,1) or not too far - OJO random part
    def __init__(self, hm, cm):
        self.hm = hm # hyst model
        self.cm = cm # creep model
        self.xold = 0.0 # Assume input start at 0 for inverse
    ##
    def update(self, valin, inverse):
        single = (type(valin)==float or type(valin)==np.float64)
        if (single):
            val = np.array([valin]) # A trick: everything is an array
        else:
            val = valin
        if(not inverse):
            x = val # A proper name
            y = np.zeros(x.shape[0])
            for tt in range(y.shape[0]):
                u1 = self.hm.update(x[tt], inverse = False)
                u2 = self.cm.update(x[tt] ,inverse = False)
                y[tt] = u1+u2
            self.xold = x[-1]
            if(single):
                return y[-1]
            else:
                return y
        else:
            y = val # A better name
            xi = np.zeros(y.shape[0]) # Output of inverse            
            for tt in range(y.shape[0]):
                for itera in range(1):
                    # OJO creep is used forward
                    u = self.cm.update(self.xold, inverse = False)
                    dum = y[tt]-u
                    if(dum<0): dum = 0.0
                    xnew = self.hm.update(dum, inverse=True)
                    self.xold = xnew
                xi[tt] = xnew
            if(single):
                return xi[-1]
            else:
                return xi
    ##
    def reset(self):
        self.hm.reset()
        self.cm.reset()
        self.xold = 0.0
        return
##
