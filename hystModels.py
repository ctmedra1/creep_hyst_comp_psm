#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright 2023 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""

import sys
import numpy as np

##
def Hfunction(x, yold, r):
    #return max(x-r, min(x+r,y))
    return max(x-r,min(x,yold)) # For positive signals
##
def evalOspList(x, popL, w):
    val = 0.0
    for nn in range(len(popL)):
        val += popL[nn].update(x)*w[nn]
    return val
##
def evalSeriesOspList(x,popL, w):
    y = np.zeros(x.shape[0])
    for tt in range(x.shape[0]):
        y[tt] = evalOspList(x[tt], popL, w)
    return y
    

class OneSidePlayOp():
    def __init__(self, r):
        self.r = r 
        self.yold = 0.0 # Last value of y, Assume starts at 0
        # yold is h0 in the article: 0 for increasing input, Umax-r for decreasing
    ##
    def update(self, xnew):
        """
        Updates the state of the operator and return the ouput value
        """
        ynew = Hfunction(xnew, self.yold, self.r)
        self.yold = ynew
        return ynew
    ##
    def reset(self):
        self.yold = 0.0 # Assume reset at 0.0
    ##
##
class MPI():
    """
    MPI class with initial value = 0.0 and ten components
    The input should be normalized in [0, 1]
    """
    def __init__(self, a_3, a_2, a_1, pho_r, tau_r, r0):
        """
        The class is made of a weighed sum of a model with
        hysteresis (play operators) and a polynomial.
        """
        #print('r0 not used for the moment')
        # The linear term is taken apart
        self.a_1 = a_1
        self.pol = np.polynomial.Polynomial(coef=(0, 0, a_2, a_3))
        ##
        N = 6
        self.r = np.arange(0, N)/float(N)
        # w-s are like b-s in the article. deltar is 1/N => divide by N
        #self.w = pho_r*np.exp(-tau_r*(self.r-r0)**2)/N # Used in gu_14
        self.w = pho_r*np.exp(-tau_r*(self.r))/N
        self.popL = []
        for nn in range(N):
            self.popL.append(OneSidePlayOp(self.r[nn]))
        ##
        self.ri = np.zeros(N)
        self.ri[0] = a_1*self.r[0]
        for nn in range(1,N):
            self.ri[nn] = a_1*self.r[nn]+(self.w[0:nn]*(self.r[nn]-self.r[0:nn])).sum()
        ##
        self.a_1i = 1.0/a_1
        self.wi = np.zeros(N)
        for nn in range(0,N): # Not very efficient but done only once
            val1 = (a_1+self.w[0:nn+1].sum())
            val2 = (a_1+self.w[0:nn].sum())
            self.wi[nn] = -self.w[nn]/val1/val2
        ##
        self.popLi = []
        for nn in range(N):
            self.popLi.append(OneSidePlayOp(self.ri[nn]))
        ##
        #self.xold = None
        self.xold = 0.0 # Assume at 0.0 for inverse
        # There is no yold because each operator has his internal yold
        ##
    def update(self, x, inverse):
        if((type(x)==float or type(x)==np.float64) and not inverse):
            val = evalOspList(x, self.popL, self.w)
            val += self.pol(x)+self.a_1*x
            return val
        elif(type(x) == np.ndarray and not inverse):
            y = evalSeriesOspList(x,self.popL, self.w)
            y += self.pol(x)+self.a_1*x
            return y
        elif((type(x)==float or type(x)==np.float64) and inverse):
            y = x # Just to name it in a more suitable way
            for itera in range(1):
                z = y-self.pol(self.xold)
                # P-1 block in fig.7 of the article
                xnew = evalOspList(z,self.popLi, self.wi)
                xnew += self.a_1i*z
                # End of P-1 block
                self.xold = xnew
            return xnew
        elif(type(x) == np.ndarray and inverse):
            y = x # Just to name the input in a more suitable way
            xi = np.zeros(y.shape[0])
            for tt in range(y.shape[0]):
                if(tt!=0):
                    self.xold = xi[tt-1]
                for itera in range(1):
                    # La iteracion es para buscar estabilidad
                    z = y[tt]-self.pol(self.xold)
                    # P-1 block in fig.7 of the article
                    xnew = evalOspList(z,self.popLi, self.wi)
                    xnew += self.a_1i*z
                    # End of P-1 block
                    self.xold = xnew
                xi[tt] = xnew
            return xi
        else:
            print('type not included in update ', type(x))
            sys.exit(0)
    def reset(self):
        # Reset initial values to 0. Operators can be used again, no memory
        self.xold = 0.0
        for nn in range(len(self.popL)):
            self.popL[nn].reset()
            self.popLi[nn].reset()
        return
##
