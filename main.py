"""
Copyright 2023 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


This program obtains the main results of the article

Creep and Hysteresis Compensation in Pressure Sensitive Mats for Improving Center-of-Pressure Measurements

by Javier Martinez-Cesteros, Carlos Medrano-Sanchez, Julian Castellanos-Ramos, Josa A. Sanchez-Duran, and Inmaculada Plaza-Garcia

accepted for publication in IEEE Sensors Journal


"""
import numpy as np
import os

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.ion()

from pneumaticExperiment import *
from copExperiment import *


if(not os.path.isdir('results')):
    os.mkdir('results')

# 1 figures 5 and 6 of the article. Output as pdf to directory results/
minname = "data/minMPICreepAddPneu2024.json"
chmodel = load_min_model(minname, T=6.0)

fname = 'data/Hysteresis_cells_pr_n_raw_Velostat_Downward.csv'
data = np.loadtxt(fname)
xexp0 = data[0,:]
raw0 = data[1,:]
yexp0 = getConductance16x16(raw0, Rserie=2181.0, nbits=10, box="malaga9x9")
#
fname = 'data/Hysteresis_cells_pr_n_raw_Velostat_Upward.csv'
data = np.loadtxt(fname)
xexp1 = data[0,:]
raw1 = data[1,:]
yexp1 = getConductance16x16(raw1, Rserie=2181.0, nbits=10, box="malaga9x9")

# Normalize. 
# It is somehow more convenient to work with normalized values
# Then we will get physical units
xmax = max(xexp0.max(), xexp1.max())
ymax = max(yexp0.max(), yexp1.max())
uexp0 = xexp0/xmax
uexp1 = xexp1/xmax
vexp0 = yexp0/ymax
vexp1 = yexp1/ymax
# Set low pressure to 0 Look at graphs to select 0.025
# A residual pressure seems to be at 0 set point, it is more likely to
# be 0. Anyway is between the limits of the accuracy of the pressure chamber
uexp0[uexp0<0.025] = 0.0
uexp1[uexp1<0.025] = 0.0

# Model output
chmodel.reset()
v0 = chmodel.update(uexp0, inverse=False)
chmodel.reset()
v1 = chmodel.update(uexp1, inverse=False)

# Hysteresis figures
for nn in range(2):
    if(nn==0):
        uexp = uexp0
        vexp = vexp0
        v = v0
        ncycle = uexp0.shape[0]//10 # 144
        outname = 'results/downward_'
    else:
        uexp = uexp1
        vexp = vexp1
        v = v1
        ncycle = uexp1.shape[0]//10 # 336
        outname = 'results/upward_'
    plt.clf()
    plt.plot(6.0*np.arange(0, ncycle),uexp[0:ncycle] * xmax, "o-")
    plt.ylabel("Pressure (psi)", size = 18)
    plt.xlabel("Time (s)", size = 18)
    plt.yticks(fontsize = 18)
    plt.xticks(fontsize = 18)
    if(nn==1):
        plt.xticks(np.arange(0, 336*6, 500), fontsize = 18)
    plt.tight_layout()
    plt.savefig(outname+"0.pdf", dpi=600)
    #
    plt.clf()
    plt.plot(6.0*np.arange(0, v.shape[0]),v * ymax, "o-")
    plt.plot(6.0*np.arange(0, vexp.shape[0]), vexp * ymax, "o-")
    plt.ylabel("Conductance (S)", size=18)
    plt.xlabel("Time (s)", size=18)
    if(nn==0):
        plt.xticks(np.arange(0, 10*ncycle*6, 400*6), fontsize = 18)
    else:
        plt.xticks(np.arange(0, 10*ncycle*6, 800*6), fontsize = 18)        
    plt.legend(["model fit", "experiment"], fontsize=18)
    ax = plt.gca()
    ax.ticklabel_format(axis='y', style='sci', scilimits = (0,0))
    ax.yaxis.offsetText.set_fontsize(18)
    plt.yticks(fontsize = 18)
    plt.tight_layout()
    plt.savefig(outname+"1.pdf", dpi=600)
    #
    plt.clf()
    plt.plot(6.0*np.arange(ncycle), v[0:ncycle] * ymax, "o-")
    plt.plot(6.0*np.arange(ncycle), vexp[0:ncycle] * ymax, "o-")
    plt.ylabel("Conductance (S)", size=18)
    plt.xlabel("Time (s)", size=18)
    plt.xticks(fontsize = 18)
    plt.legend(["model fit", "experiment"], fontsize=18)
    ax = plt.gca()
    ax.ticklabel_format(axis='y', style='sci', scilimits = (0,0))
    if(nn==1):
        tick_spacing = 500
        ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
    ax.yaxis.offsetText.set_fontsize(18)
    plt.yticks(fontsize = 18)
    plt.tight_layout()
    plt.savefig(outname+"2.pdf", dpi=600)
    #
    plt.clf()
    plt.plot(6.0*np.arange(4*ncycle,5*ncycle), v[4*ncycle:5*ncycle] * ymax, "o-")
    plt.plot(6.0*np.arange(4*ncycle,5*ncycle), vexp[4*ncycle:5*ncycle] * ymax, "o-")
    plt.ylabel("Conductance (S)", size=18)
    plt.xlabel("Time (s)", size=18)
    plt.xticks(fontsize = 18)
    plt.legend(["model fit", "experiment"], fontsize=18)
    ax = plt.gca()
    ax.ticklabel_format(axis='y', style='sci', scilimits = (0,0))
    if(nn==1):
        tick_spacing = 500
        ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
    ax.yaxis.offsetText.set_fontsize(18)
    plt.yticks(fontsize = 18)
    plt.tight_layout()
    plt.savefig(outname+"3.pdf", dpi=600)

print()
print('#'*10+ ' Model parameters - experiments with pneumatic platform')

print('Unit less model parameters: ')
print('First with normalized values: pressure and conductances were first divided')
print('by their maximum values in the experiment (xmax and ymax in the code)')

# Values of the model found
minname = "data/minMPICreepAddPneu2024.json"

with open(minname, 'r') as fp:
    dic = json.load(fp)
a_30, a_20, a_10, rho_0, tau_r0, r0_0 = dic['a_3min'], dic['a_2min'], dic['a_1min'], dic['rho_min'], dic['tau_rmin'], dic['r0_min']
w0, l0 = np.array(dic['wmin']), np.array(dic['lmin'])

print('a1 ', a_10)
print('a2 ', a_20)
print('a3 ', a_30)
print('rho ', rho_0)
print('tau ', tau_r0)
print('l1 ', l0[0])
print('l2 ', l0[1])
print('w1 ', w0[0])
print('w2 ', w0[1])

print()
print('Model parameters in physical units, after multipying by the suitable factors: ')
a_1 = a_10 * ymax / xmax
a_2 = a_20 * ymax / xmax / xmax
a_3 = a_30 * ymax / xmax / xmax / xmax
rho = rho_0*ymax/xmax
tau_r = tau_r0/xmax
l = l0.copy()
w = w0*ymax/xmax

print('a1 ', a_1)
print('a2 ', a_2)
print('a3 ', a_3)
print('rho ', rho)
print('tau ', tau_r)
print('l1 ', l[0])
print('l2 ', l[1])
print('w1 ', w[0])
print('w2 ', w[1])

# Get SSEN
# Descendent hysteresis cycles
sse0 = ((vexp0-v0)**2).sum()/v0.shape[0]

# Asecendant hysteresis cycles 
# Each experiment is different with time in between 
# for the sensor to settle
sse1 = ((vexp1-v1)**2).sum()/v1.shape[0]

# SSEN shown in the article is the average
ssen = 0.5*(sse0+sse1)
ssen *= ymax*ymax  # Change to physical units (S^2)
print()
print('## Fit of pneumatic experiment')
print('SSEN ', ssen)

# 2. Show EU for all the volunteers, experiments and models together with 
# scaling factor and a summary of the results (which is the information
# of figure 8 in total). The other could be extracted from the values
# shown in the standard output.

print()
print('#'*10+ ' COP experiment EU by volunteer and experiment for each model')
print()
print('Volunteer experiment EU-COP EU-CHC EU-SCHC')

chmodel = load_min_model(minname, T = 1/100.0) # In the Cop Experiment frequency was 100 Hz
dirname = 'data/'
electrode = 'InterDigit'
volunteers = ['ID1', 'ID2', 'ID3','ID4']
experiments = ['RightLeg', 'LeftLeg', 'Rotation']

# Multiplying factors of conductance for SCHC
# They were found using conductance normalized for each CoP experiment
# The true factor is printed below (without normalizing neither
# the values in the pneumatic experiments nor the values in the CoP exp)

factors = {}

factors['LeftLeg'] = {'ID1':1.2, 'ID2':1.25, 'ID3': 1.2, 'ID4': 1.15}
factors['RightLeg'] = {'ID1':1.05, 'ID2':1.2, 'ID3': 1.25, 'ID4': 1.2}
factors['Rotation'] = {'ID1':1.2, 'ID2':1.25, 'ID3': 1.3, 'ID4': 1.25}



eu_prop = []
eu_chc = []
eu_schc = []

for experiment in sorted(experiments):
    for vol in sorted(volunteers):
        ## Read times
        fname = dirname+'/'+vol+'/'+experiment+'/'+'inifin_'+electrode+'.txt'
        with open(fname,'r') as f:
            s = f.read()
        dic = eval(s)
        tg1, tg2 = dic['psm_ini'], dic['psm_fin']
        # Pasco at 500 Hz so downsampling for cm
        tcm1, tcm2, tcm_step = dic['pasco_ini'], dic['pasco_fin'], dic['pasco_step']
        ## Load Pasco force platform
        fname = dirname+'/'+vol+'/'+experiment+'/'+'Pasco_'+electrode+'_'+experiment+'.csv'
        data = pd.read_csv(fname, sep=";", decimal=",")
        xcm = data['Xc (cm) Serie Nº 1'].to_numpy() 
        ycm = data['Yc (cm) Serie Nº 1'].to_numpy()
        # Extract 30 s windows
        xcm = xcm[tcm1:tcm2:tcm_step]
        ycm = ycm[tcm1:tcm2:tcm_step]
        xcm, ycm = xcm-xcm.mean(), ycm-ycm.mean()
        # Force (Weight)
        force_series = data['Fuerza vertical (N) Serie Nº 1'].to_numpy()
        force_real = force_series[dic['pasco_ini']:dic['pasco_fin']].mean()
        ## PSM conductance
        fname = dirname+'/'+vol+'/'+experiment+'/'+'Malla_'+electrode+'_'+experiment+'_'+'conduct_simple_filt.npy'
        g = np.load(fname)
        g3 = np.rot90(g, k=1, axes=(1,2)) # Rotate to align axis with Force Platform
        # PROP model pressure and conductance are just proportional
        pkk = g3[0:tg2,:]
        xkk, ykk = pressure2CopSeries(pkk, d = 2.0)
        xkk, ykk = xkk[tg1:tg2], ykk[tg1:tg2]
        xkk, ykk = xkk-xkk.mean(), ykk-ykk.mean()
        eu = np.sqrt(((xcm-xkk)**2+(ycm-ykk)**2).sum())/xcm.shape[0]
        eu_prop.append(eu)
        # CHC: apply model compensation without scaling
        # The model is obtained with the conductance normalized to the 
        # maximum value of the experiment with the pneumatic platform
        # So divide by ymax before
        g = g3/ymax
        p = np.zeros((tg2,g.shape[1], g.shape[2])) # Pressure
        for ii in range(g.shape[1]):
            for jj in range(g.shape[2]):
                chmodel.reset()
                p[:,ii,jj] = chmodel.update(g[0:tg2,ii,jj], inverse = True)
        xcm_chc, ycm_chc = pressure2CopSeries(p, d = 2.0)
        xcm_chc, ycm_chc = xcm_chc[tg1:tg2], ycm_chc[tg1:tg2]
        xcm_chc, ycm_chc = xcm_chc-xcm_chc.mean(), ycm_chc-ycm_chc.mean()
        eu = np.sqrt(((xcm-xcm_chc)**2+(ycm-ycm_chc)**2).sum())/xcm.shape[0]
        eu_chc.append(eu)
        # SCHC: apply model compensation with scaling
        # 
        f = factors[experiment][vol]
        g3max = g3[0:tg2].max() 
        g = g3/g3max*f
        p = np.zeros((tg2,g.shape[1], g.shape[2])) # Pressure
        for ii in range(g.shape[1]):
            for jj in range(g.shape[2]):
                chmodel.reset()
                p[:,ii,jj] = chmodel.update(g[0:tg2,ii,jj], inverse = True)
        xcm_schc, ycm_schc = pressure2CopSeries(p, d = 2.0)
        xcm_schc, ycm_schc = xcm_schc[tg1:tg2], ycm_schc[tg1:tg2]
        xcm_schc, ycm_schc = xcm_schc-xcm_schc.mean(), ycm_schc-ycm_schc.mean()
        eu = np.sqrt(((xcm-xcm_schc)**2+(ycm-ycm_schc)**2).sum())/xcm.shape[0]
        eu_schc.append(eu)
        print(experiment, vol, eu_prop[-1], eu_chc[-1], eu_schc[-1])
        # Force in PSM: multiply pressure by area and by a factor that 
        # changes to PSI and to Pa
        force = p[tg1:tg2,:,:].mean(0).sum()*PFACTOR*LX*LY
        print('Force (FP) and force (PSM - SCHC) all in Newtons ', force_real, force)
        print('Scaling factor ', f*ymax/g3max)


print("EU: Total average")
print("EU-PROP ", np.mean(eu_prop))
print("EU-CHC ", np.mean(eu_chc))
print("EU-SCHC ", np.mean(eu_schc))
