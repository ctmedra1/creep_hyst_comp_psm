#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright 2023 Carlos Medrano-Sanchez (ctmedra@unizar.es,
ctmedra@gmail.com) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import sys

import numpy as np
import pandas as pd
import pickle
import json


import hystModels
import creepModels
import hystCreepModels
##


def load_min_model(minname, T):
    with open(minname, 'r') as fp:
        dic = json.load(fp)
    a_30, a_20, a_10, rho_0, tau_r0, r0_0 = dic['a_3min'], dic['a_2min'], dic['a_1min'], dic['rho_min'], dic['tau_rmin'], dic['r0_min']
    w0, l0 = np.array(dic['wmin']), np.array(dic['lmin'])
    costmin = dic['costmin']
    mpi = hystModels.MPI(a_30, a_20, a_10, rho_0, tau_r0, r0_0)
    cre = creepModels.CreepOpArray(l0, w0, T)
    chmodel = hystCreepModels.HystCreepAdd(mpi, cre)
    return chmodel
##
def getConductance16x16(rawMap, Rserie = 2000.0, nbits = 12, box = None):
    """
    Function for our Velostat Pressure Sensitive Mat.
    It is used to get conductance from the raw value of the ADC in a scan (rawMap)
    """
    R1 = Rserie # Voltage divider resistance (connected to power Vref)
    lsb = 1.0/(2**nbits-1)
    val = rawMap*lsb
    #conductance = (1.0/val-1)/R1
    res = R1/(1.0/val-1)
    if(box=='black'):
        # Se comprueba que el ajuste es como tener una R serie del MUX y una R entada del ADC de 123 KOmhs
        epsilon = 1e-6 
        rin = 123e3 
        rmux = 145.0
        # evitamos que el ajuste no tenga sentido
        res[res<rmux] = rmux+epsilon
        res[res>rin] = rin-epsilon
        resReal = rin*res/(rin-res)-rmux
    elif (box == 'velo3_002'):
        # Se comprueba que el ajuste es como tener una R serie del MUX y una R entada del ADC de 123 KOmhs
        epsilon = 1e-6 
        rin = 131150.0
        rmux = 126.5
        # evitamos que el ajuste no tenga sentido
        res[res<rmux] = rmux+epsilon
        res[res>rin] = rin-epsilon
        resReal = rin*res/(rin-res)-rmux
    elif (box == 'velo3_003'):
        # Se comprueba que el ajuste es como tener una R serie del MUX y una R entada del ADC de 123 KOmhs
        epsilon = 1e-6 
        rin = 143158.0
        rmux = 131.1
        # evitamos que el ajuste no tenga sentido
        res[res<rmux] = rmux+epsilon
        res[res>rin] = rin-epsilon
        resReal = rin*res/(rin-res)-rmux
    elif (box == 'malaga9x9'):
        epsilon = 1e-6 
        #rin = 143158.0 Asumo infinito
        rmux = 56.88
        res[res<rmux] = rmux+epsilon
        res = res-rmux
        resReal = res
    elif(box is None):
        resReal = res
    else:
        print('box unkown')
        sys.exit(0)
    conductance = 1.0/resReal
    return conductance
##
